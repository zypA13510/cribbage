sap.ui.define([
	"sap/m/MultiComboBox",
	"sap/ui/core/Item"
], function(MultiComboBox, Item) {
	"use strict";
	
	var suits = ["s", "h", "c", "d"];
	var fnNumberToCard = function(iNumber) {
		return 1 < iNumber && iNumber <= 10 ? String(iNumber) : {
			1: "A",
			11: "J",
			12: "Q",
			13: "K"
		}[iNumber];
	};
	var fnSuitToSymbol = function(sCode) {
		return {
			"s": String.fromCharCode(9824),
			"h": String.fromCharCode(9829),
			"c": String.fromCharCode(9827),
			"d": String.fromCharCode(9830)
		}[sCode];
	};
	
	var oCardSelector = MultiComboBox.extend("zyp.cribbage.util.CardSelector", /** @lends zyp.cribbage.util.CardSelector.prototype */ {
		constructor: function(sId, mProperties) {
			if (typeof sId !== "string" && sId !== undefined) {
				mProperties = sId;
				sId = mProperties && mProperties.id;
			}
			var _items = [];
			for (var i in suits) {
				var suit = suits[i];
				for (var j = 1; j <= 13; j++) {
					_items.push(new Item({
						key: suit + j,
						text: fnNumberToCard(j) + " " + fnSuitToSymbol(suit)
					}));
				}
			}
			var _mProperties = jQuery.extend({}, mProperties, {
				items: _items
			});
			MultiComboBox.apply(this, [sId, _mProperties]);
		},
		renderer: "sap.m.MultiComboBoxRenderer"
	});
	/*
	oCardSelector.prototype.addItem =
	oCardSelector.prototype.bindItems =
	oCardSelector.prototype.destroyItems =
	oCardSelector.prototype.insertItem =
	oCardSelector.prototype.removeAllItems =
	oCardSelector.prototype.removeItem =
	oCardSelector.prototype.unbindItems =
	function() {};
	*/
	return oCardSelector;
});