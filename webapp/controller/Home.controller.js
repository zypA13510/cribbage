sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/m/MessageBox"
], function (Controller, MessageBox) {
	"use strict";
	
	// Card code:
	// suit + number
	// suit = s/h/c/d (spade, heart, club, diamond)
	// e.g. c13 = club King, and s1 = spade Ace
	var suits = ["s", "h", "c", "d"];
	
	/**
	 * Calculate the number of 15s
	 * @param {Array<string>} cards An array of card code.
	 * @async
	 * @return {number} the number of 15s found.
	 */
	var fnCalc15s = function(cards) {
		// Turn this into async function
		return new Promise(function(resolve) {
			// initialize
			var aQueue = [new Set(cards)];
			var iCount = 0;
			// start searching until nothing more to search
			while (aQueue.length) {
				// get the first item from worklist
				var setCurrent = aQueue[0];
				aQueue = aQueue.slice(1);
				var iSum = 0;
				setCurrent.forEach(function(card) {
					// get number part from card code and add to iSum
					var iCard = Number(card.substring(1));
					iSum += iCard > 10 ? 10 : iCard;
				});
				if (iSum > 15) {
					setCurrent.forEach(function(card) {
						// make a copy
						var setNew = new Set(setCurrent);
						// remove one element
						setNew.delete(card);
						// check for duplicate
						if (!aQueue.find(function(item) {
							return eqSet(item, setNew);
						})) {
							// add the new item to worklist
							aQueue.push(setNew);
						}
					});
				} else if (iSum == 15) {
					// found match
					iCount++;
				} // else (iSum < 15) nothing to do
			}
			// async return
			resolve(iCount);
		});
	};
	
	/**
	 * Calculate the number of pairs
	 * @param {Array<string>} cards An array of card code.
	 * @async
	 * @return {number} the number of pairs found.
	 */
	var fnCalcPairs = function(cards) {
		// Turn this into async function
		return new Promise(function(resolve) {
			// cut the suit code and leave only number part
			var aCards = cards.map(function(sCode) {
				return Number(sCode.substring(1));
			});
			// count cards at each number
			var aNumbers = new Array(13).fill(0);
			aCards.forEach(function(card) {
				aNumbers[card - 1]++;
			});
			// calculate
			var iSum = 0;
			for (var i = 0; i < 13; i++) {
				iSum += (aNumbers[i] - 1) * aNumbers[i] / 2;
			}
			resolve(iSum);
		});
	};
	
	/**
	 * Calculate the score of longest runs
	 * @param {Array<string>} cards An array of card code.
	 * @async
	 * @return {number} the score of longest runs.
	 */
	var fnCalcRuns = function(cards) {
		// Turn this into async function
		return new Promise(function(resolve) {
			// cut the suit code and leave only number part
			var aCards = cards.map(function(sCode) {
				return Number(sCode.substring(1));
			});
			// count cards at each number
			var aNumbers = new Array(13).fill(0);
			aCards.forEach(function(card) {
				aNumbers[card - 1]++;
			});
			var iSum = 0;
			for (var i = 0; i < 9; i++) {
				iSum += 5 * (aNumbers[i] * aNumbers[i + 1] * aNumbers[i + 2] * aNumbers[i + 3] * aNumbers[i + 4]);
			}
			// no runs of 5 found
			if (iSum == 0) {
				for (var i = 0; i < 10; i++) {
					iSum += 4 * (aNumbers[i] * aNumbers[i + 1] * aNumbers[i + 2] * aNumbers[i + 3]);
				}
				// no runs of 4 found
				if (iSum == 0) {
					for (var i = 0; i < 11; i++) {
						iSum += 3 * (aNumbers[i] * aNumbers[i + 1] * aNumbers[i + 2]);
					}
					// if (iSum == 0) no run of at least 3 in cards
				}
			}
			// async return
			resolve(iSum);
		});
	};
	
	/**
	 * Calculate the score of flushes
	 * @param {Array<string>} hand An array of card code representing my hand.
	 * @param {Array<string>} shared An array of card code representing community cards.
	 * @async
	 * @return {number} the score of flushes.
	 */
	var fnCalcFlushes = function(hand, shared) {
		// Turn this into async function
		return new Promise(function(resolve) {
			if (hand.every(function(card) {
				// compare suit code of current card to the first card
				return card[0] == hand[0][0];
			})) {
				if (shared.every(function(card) {
					// compare suit code of current card (shared) to the first card
					return card[0] == hand[0][0];
				})) {
					// 5 points if hand and shared are all from same suit
					resolve(5);
				} else {
					// 4 points if hand are all from same suit
					resolve(4);
				}
			} else {
				// no point for inconsistent hand
				resolve(0);
			}
		});
	};
	
	/**
	 * Calculate the number of nobs
	 * @param {Array<string>} hand An array of card code representing my hand.
	 * @param {Array<string>} shared An array of card code representing community cards.
	 * @async
	 * @return {number} the number of nobs.
	 */
	var fnCalcNobs = function(hand, shared) {
		// Turn this into async function
		return new Promise(function(resolve) {
			var setHand = new Set(hand);
			var iCount = 0;
			shared.forEach(function(card) {
				// suit code + 11 = jack of the suit
				if (setHand.has(card[0] + 11)) {
					iCount++;
				}
			});
			resolve(iCount);
		});
	};
	
	/**
	 * Utility function for comparing two Sets
	 * @param {Set<>} setA
	 * @param {Set<>} setB
	 * @return {boolean} true if setA is identical to setB, false otherwise.
	 */
	function eqSet(setA, setB) {
		if (setA.size != setB.size) return false;
		for (var a of setA) if (!setB.has(a)) return false;
		return true;
	}
	
	return Controller.extend("zyp.cribbage.controller.Home", {
		onNext: function(oEvent) {
			var aMyHand = this.getView().getModel("local").getProperty("/cards1");
			var aCommunity = this.getView().getModel("local").getProperty("/cards2");
			var aCombined = aMyHand.concat(aCommunity);
			var aPromises = Promise.all([
				fnCalc15s(aCombined),
				fnCalcPairs(aCombined),
				fnCalcRuns(aCombined),
				fnCalcFlushes(aMyHand, aCommunity),
				fnCalcNobs(aMyHand, aCommunity)
			]);
			// execute after all async calls returned
			aPromises.then(function(results) {
				MessageBox.information(
					"15s found: " + results[0]
					+ "\npairs found: " + results[1]
					+ "\nruns score: " + results[2]
					+ "\nflushes score: " + results[3]
					+ "\nnobs found: " + results[4]
					+ "\ntotal score: " + (
						2 * results[0] +
						2 * results[1] +
						1 * results[2] +
						results[3] +
						1 * results[4]
					)
				);
			});
		}
    });
});